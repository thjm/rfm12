//
// rfm12Morse.ino
//
// Test/example program for generating OOK (Morse signs) using RFM12 module and
// the class Morse (which has to be properly modified for inheritance).
//
// See also (post by MartinR):
// - https://openenergymonitor.org/forum-archive/node/3369.html
//

// pin where nSEL of the RFM12 module is connected to (Pin 10 is also SS)
const int kRFMSELPIN = 10;

#include "RFM12Morse.h"

RFM12Morse *rfm12Morse = NULL;;

void setup() {

  Serial.begin(9600);
  Serial.println("\n[rfm12Morse]");

  rfm12Morse = new RFM12Morse(kRFMSELPIN);

  rfm12Morse->setSpeed(25);
}

static unsigned int sequenceCtr = 0;

void loop() {

  sequenceCtr++;

  Serial.print("loop: ");
  Serial.println(sequenceCtr);

  rfm12Morse->print(F("ka qam de DC2IP = "));
  rfm12Morse->print(F("nr "));
  rfm12Morse->print(sequenceCtr);
  rfm12Morse->print(F(" = "));
  rfm12Morse->print(F("Vbat 50 = "));
  rfm12Morse->print(F("pse qsl +"));

  delay(10000);
}
