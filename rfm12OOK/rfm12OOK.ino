
//
// rfm12OOK.ino - another OOK test example with RFM12(B)
//
// see also: https://openenergymonitor.org/forum-archive/node/3369.html
//


#include <SPI.h>

// pin where nSEL of the RFM12 module is connected to (Pin 10 is also SS)
#define RFMSELPIN   10

void setup() {

  //Serial.begin(57600);
  Serial.begin(9600);
  Serial.println("\n[POSA Plug Demo]");

  pinMode(RFMSELPIN, OUTPUT);
  digitalWrite(RFMSELPIN,HIGH);

  // start the SPI library:
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(0);
  SPI.setClockDivider(SPI_CLOCK_DIV2);

  // initialise RFM12
  delay(200); // wait for RFM12 POR
  rfm_write(0x0000); // clear SPI
  rfm_write(0xCA83); // non-sensitive reset
  rfm_write(0x80D7); // 433MHz band
  rfm_write(0xA640); // set frequency to 434.0000MHz (adjust to suit device)
  rfm_write(0x9850); // max power
  rfm_write(0x821d); // Turn on crystal and synthesizer
}

void loop() {

  Serial.println("on");
#if 0
  POSA_Plug(1);

  delay(2000);
#else
  rfm_write(0x823d);

  delay(2000);

  Serial.println("off");

  rfm_write(0x821d);

  delay(200);
#endif
}

void ookPulse(int on, int off) {

  rfm_write(0x823d);
  delayMicroseconds(on+3);
  rfm_write(0x821d);
  delayMicroseconds(off-43);
}

void POSA_Plug(byte plugNo) {

  if (plugNo==1) {
    ookPulse(200,400);
    ookPulse(200,400);

    ookPulse(400,200);
    ookPulse(400,200);
    ookPulse(400,200);

    ookPulse(200,400);
    ookPulse(200,400);
    ookPulse(200,400);
  }

  delay(11); // approximate
}

// write a command to the RFM12
word rfm_write(word cmd) {

  word result;

  digitalWrite(RFMSELPIN,LOW);
  result=(SPI.transfer(cmd>>8)<<8) | SPI.transfer(cmd & 0xff);
  digitalWrite(RFMSELPIN,HIGH);

  return result;
}
