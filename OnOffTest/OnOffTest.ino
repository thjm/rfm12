
//
// OnOffTest.ino
//
// Test program for the RFM12 module for simple OOK
//
// Inspired by the fs20demo test program from JeeLib/examples.
// See also: https://github.com/jcw/jeelib/blob/master/examples/RF12/fs20demo/fs20demo.ino
//
// Note that RFM12B radios are not really designed for OOK (on-off keying),
// but this can be simulated anyway by simply turning the transmitter on and
// off via the SPI interface. Doing so takes about 25 usecs, so the delays
// used for encoding simple bit patterns need to be adjusted accordingly.
//
// Proposed wiring scheme:
//
//  SDO  - D12 (MISO)          nSEL - D10 (SS)
//  nIRQ - D2 (INT0)           SCK  - D13 (SCK)
//  FSK  - via 10k to Vcc      SDI  - D11 (SCK)
//       -                     nINT/VDI -
//  CLK  -                     GND  - GND
//  nRES -                     Vdd  - Vcc
//  GND  - GND                 ANN  - Antenna
//
//  (see also:
//   - https://baunotizen.wordpress.com/2014/01/13/rfm12-funkmodule-mit-arduino-ansprechen/)
//   - JeeLib/RF12.cpp)
//
// Note on differences between RFM12 and RFM12B:
// (Source: http://jeelabs.org/2009/05/06/rfm12-vs-rfm12b-revisited/index.html)
// - The RFM12 works up to 5V, the RFM12B only up to 3.8V
//   (but it won't be damaged by 5V).
// - The RFM12 needs a pull-up resistor of 10..100 KΩ on the FSK/DATA/nFFS pin,
//   the RFM12B doesn't.
// - The RFM12 can only sync on the hex 2DD4 2-byte pattern, the RFM12B can
//   sync on any value for the second byte, not just D4.
//
// - The B version can be set to a single sync byte.
// - The 'PLL setting command' (CCxx), which only exists on the RFM12B.
// - Different values for RX sensitivity and TX output power.
// - A slightly different formula to calculate the time of the Wake-Up Timer.
//

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/crc16.h>
#include <avr/eeprom.h>
#include <avr/sleep.h>

#include <JeeLib.h>

// RF12 configuration area, from RF12demo.ino
typedef struct {
    byte nodeId;            // used by rf12_config, offset 0
    byte group;             // used by rf12_config, offset 1
    byte format;            // used by rf12_config, offset 2
    byte hex_output   :2;   // 0 = dec, 1 = hex, 2 = hex+ascii
    byte collect_mode :1;   // 0 = ack, 1 = don't send acks
    byte quiet_mode   :1;   // 0 = show all, 1 = show only valid packets
    byte spare_flags  :4;
    word frequency_offset;  // used by rf12_config, offset 4
    byte pad[RF12_EEPROM_SIZE-8];
    word crc;

} RF12Config;

static RF12Config config;

// from RF12.cpp
#if RF12_COMPAT
 #define crc_initVal     0x1D0F
 #define crc_endVal      0x1D0F
 #define crc_update      _crc_xmodem_update
#else
 #define crc_initVal     ~0
 #define crc_endVal      0
 #define crc_update      _crc16_update
#endif

// try to write some info into config EEPROM (of AVR)
//
// see also (RF12.cpp):
// - rf12_configDump()
// - rf12_configSilent()
//
// For the EEPROM layout, see http://jeelabs.net/projects/jeelib/wiki/RF12demo
//
void writeRFM12config() {

  config.nodeId = 0;
  config.group = 212; // only RFM12
  config.format = RF12_EEPROM_VERSION;
  config.hex_output = 0;   // 0 = dec, 1 = hex, 2 = hex+ascii
  config.collect_mode = 1;   // 0 = ack, 1 = don't send acks
  config.quiet_mode = 0;   // 0 = show all, 1 = show only valid packets
  config.spare_flags = 0;
  config.frequency_offset = 1600; // valid 1220 .. 1916
  for ( int i=0; i<RF12_EEPROM_SIZE-8; ++i )
    config.pad[i] = 0;
  config.crc = 0;

  uint8_t band = RF12_433MHZ;
  config.nodeId |= (band << 6);

  cli();

  eeprom_write_byte(RF12_EEPROM_ADDR + 0, config.nodeId);
  eeprom_write_byte(RF12_EEPROM_ADDR + 1, config.group);
  eeprom_write_byte(RF12_EEPROM_ADDR + 2, config.format);
  eeprom_write_byte(RF12_EEPROM_ADDR + 3, *(&config.format + 1));
  eeprom_write_word((uint16_t*) (RF12_EEPROM_ADDR + 4), config.frequency_offset);

  sei();

  // calculate CRC
  Serial.print("Config space in EEPROM has lenght= ");
  Serial.println((int)RF12_EEPROM_SIZE);

  uint16_t crc = ~0;
  for (uint8_t i = 0; i < RF12_EEPROM_SIZE; ++i) {
    byte e = eeprom_read_byte(RF12_EEPROM_ADDR + i);
    crc = crc_update(crc, e);
  }

  config.crc = crc;

  cli();

  eeprom_write_word((uint16_t*)(RF12_EEPROM_ADDR + RF12_EEPROM_SIZE - 2), config.crc);

  sei();
}

void setup() {

  Serial.begin(9600);
  Serial.println("\n[OnOffTest]");

#if 0
  // write 0x00 to config space in EEPROM
  for ( int i=0; i<RF12_EEPROM_SIZE; ++i )
    eeprom_write_byte(RF12_EEPROM_ADDR + i, 0);
#else
  writeRFM12config();
#endif

  // use values stored in EEPROM
  //rf12_configSilent();

  rf12_initialize(0, RF12_433MHZ, 212);

  rf12_configDump();
}

void sendBit(int b) {

  // Copied note from JeeLib example:
  //  Timing values empirically obtained, and used to adjust for on/off
  //  delay in the RF12. The actual on-the-air bit timing we're after is
  //  600/600us for 1 and 400/400us for 0 - but to achieve that the RFM12B
  //  needs to be turned on a bit longer and off a bit less. In addition
  //  there is about 25 uS overhead in sending the on/off command over SPI.
  //  With thanks to JGJ Veken for his help in getting these values right.
  int width = b ? 600 : 400;

  rf12_onOff(1);
  delayMicroseconds(width + 150);
  rf12_onOff(0);
  delayMicroseconds(width - 200);
}

void loop() {

#if 0
  sendBit(1);
  delay(200);
  sendBit(0);
  delay(200);
#else

  int width = 200;

  rf12_onOff(1);
  //delayMicroseconds(width + 150);
  delay(width);
  rf12_onOff(0);
  //delayMicroseconds(width - 200);
  delay(width);
  rf12_onOff(1);
  //delayMicroseconds(width + 150);
  delay(width);
  rf12_onOff(0);
  //delayMicroseconds(width - 200);
  delay(width);

  delay(1000);
#endif

  rf12_configDump();

  Serial.println("Loop done!");
}
