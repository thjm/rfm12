README.md for the RFM12 Project

# RFM12 based transmission example, using Arduino

# Directories

## OnOffTest

```OnOffTest.ino``` - Test program for the RFM12 module for simple OOK

Inspired by the ```fs20demo``` test program from JeeLib/examples.
See also: https://github.com/jcw/jeelib/blob/master/examples/RF12/fs20demo/fs20demo.ino

## rfm12Morse

```rfm12Morse.ino``` - Test program for generating OOK (Morse signs)

The RFM12 module and the class Morse (which has to be properly modified
for inheritance) are used here.

## rfm12OOK

```rfm12OOK.ino``` - another OOK test example with RFM12(B)

## rfm22RTTY

# Requirements

- Class ```RFMMorse``` contained in https://thjm@github.com/thjm/Sketches.

- Class ```rfm22``` from https://github.com/niloc132/arduino-rfm22

  -> Recommended: Class ```rfm22``` from http://ukhas.org.uk/guides:rfm22b

  => https://github.com/jamescoxon/Misc-Projects/tree/master/RFM22

# Links

* JeeLib, library for RFM12/RFM12B based radio nodes:

  https://github.com/jcw/jeelib

  - http://jeelabs.org/2009/05/06/rfm12-vs-rfm12b-revisited/index.html)
  - http://jeelabs.net/projects/jeelib/wiki/RF12demo

- https://baunotizen.wordpress.com/2014/01/13/rfm12-funkmodule-mit-arduino-ansprechen/)

- https://openenergymonitor.org/forum-archive/node/3369.html
- https://openenergymonitor.org/forum-archive/node/3369.html

* Ava High Altitude Balloon Project

  -> Interrupt Driven RTTY Transmission on Arduino/AVR
     - http://ava.upuaut.net/?p=408

  -> Getting started with the NTX2B and the Arduino
     - http://ava.upuaut.net/?p=617

- http://www.engblaze.com/microcontroller-tutorial-avr-and-arduino-timer-interrupts/

* RTTY with RFM22B

  Also other modes like: Hellschreiber, DominoEX

  - http://ukhas.org.uk/guides:rfm22b

  - https://github.com/jonsowman/librtty

* RFM22 library for Arduino

  - https://github.com/niloc132/arduino-rfm22

* RadioHead Packet Radio library for embedded microprocessors

  "drivers" RH_RF22, RH_RF24, RH_RF69, ..., RH_ASK

  - http://www.airspayce.com/mikem/arduino/RadioHead/

==============================================================================
